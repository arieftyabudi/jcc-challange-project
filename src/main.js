import Vue from 'vue';
import App from './App.vue';
import './assets/form.css';
import VueFormulate from '@braid/vue-formulate';

Vue.use(VueFormulate, {
  validationNameStrategy: ['validationName', 'label', 'names', 'type'],
  locales: {
    en: {
      required({ name }) {
        return `Kolom ${name} Tidak Boleh Kosong.`;
      },
      number({ name }) {
        return `Kolom ${name} Hanya Bisa Di Isi Oleh Angka Saja`;
      },
      alpha({ name }) {
        return `Kolom ${name} Hanya Bisa Di Isi Oleh Kombinasi Huruf Saja`;
      },
    },
  },
  // uploader: async function (file, progress, error, options) {
  //   try {
  //     const formData = new FormData();
  //     formData.append('file', file);
  //     const result = await fetch(options.uploadUrl, {
  //       method: 'POST',
  //       body: formData,
  //     });
  //     progress(100); // (native fetch doesn’t support progress updates)
  //     return await result.json();
  //   } catch (err) {
  //     error('Unable to upload file');
  //   }
  // },
});

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
}).$mount('#app');
